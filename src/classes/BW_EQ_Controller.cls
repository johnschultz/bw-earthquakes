/**
 * @author: John Schultz
 * @date: 2016-05-28
 * @description: Controller for BW Earthquake page
 * @log:
 *	-----------------------------------------------------------------------------
 *	Developer			Date			Description
 *	-----------------------------------------------------------------------------
 *	John Schultz		2016-05-28		Newly created
 */
public class BW_EQ_Controller {
	
	// constructor
	public BW_EQ_Controller() {}
	
	/**
	* @author: John Schultz
	* @date: 2016-05-28
	* @description: Method for retrieving the google api custom setting.
	* @return: Response wrapper object.
	*/
	@AuraEnabled
	public static Response getGoogleApiKey() {
		Response r = new Response();
		r.isSuccess = false;
		
		try {
			BW_EQ_Google_API__c cs = BW_EQ_Google_API__c.getValues('API Key');
			r.isSuccess = true;
			r.msg = cs.Value__c;
		}
		catch (Exception e) {
			r.msg = e.getMessage();
		}
		
		return r;
	}
	
	
	@AuraEnabled
	public static Response geocodeLocation(String locationText) {
		Response r = new Response();
		BW_EQ_Google_API__c cs = BW_EQ_Google_API__c.getValues('API Key');
		String encodedLocation = EncodingUtil.urlEncode(locationText, 'UTF-8');
		String geoCodeUrl = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + encodedLocation + '&key=' + cs.Value__c;
		
		r.isSuccess = false;
		
		try {
			HttpRequest req = new HttpRequest();
			req.setEndpoint(geoCodeUrl);
			req.setMethod('GET');
			
			Http http = new Http();
			HttpResponse res = http.send(req);
			
			if (res.getStatusCode() == 200) {
				r.isSuccess = true;
				r.msg = res.getBody();
			}
			else {
				r.msg = res.getStatus();
			}
		}
		catch (Exception e) {
			r.msg = e.getMessage();
		}
		
		return r;
	}
	
	
	@AuraEnabled
	public static String getEarthquakes(Decimal north, Decimal south, Decimal east, Decimal west, Boolean isTop10) {
		BW_EQ_Google_API__c cs = BW_EQ_Google_API__c.getValues('GeoNames Username');
		String eqUrl = 'http://api.geonames.org/earthquakesJSON?north='+north+'&south='+south+'&east='+east+'&west='+west+'&username=' + cs.Value__c;
		
		if (isTop10 == true) {
			eqUrl += '&maxRows=10';
			
			DateTime yearAgo = DateTime.now().addYears(-1);
			String yearAgoFormatted = yearAgo.format('yyyy-MM-dd');
			
			eqUrl += '&date=' + yearAgoFormatted;
		}
		
		HttpRequest req = new HttpRequest();
		req.setEndpoint(eqUrl);
		req.setMethod('GET');
		
		Http http = new Http();
		HttpResponse res = http.send(req);
		return res.getBody();
	}
	
	
	
	public class Response {
		@AuraEnabled public String msg {get; set;}
		@AuraEnabled public Boolean isSuccess {get; set;}
	}
}