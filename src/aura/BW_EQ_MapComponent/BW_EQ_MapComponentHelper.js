({
	addMarkers: function(map, quakes, component) {
		if (quakes && quakes.length) {
			var bounds = new google.maps.LatLngBounds();
			
			for (var i = 0; i < quakes.length; i++) {
				var q = quakes[i];
				var ll = new google.maps.LatLng(q.lat, q.lng);
				var marker = new google.maps.Marker({
					position: ll,
					map: map
				});
				
				bounds.extend(ll);
			}
			
			map.fitBounds(bounds);
		}
		else {
			this.displayError('No earthquake data coul be found for that location.', component);
		}
	},
	
	displayError: function(error, component) {
		component.set('v.error', error);
		component.set('v.showError', true);
	},
	
	getEarthquakes: function(component) {
		var action = component.get('c.getEarthquakes');
		var locationGeoInfo = component.get('v.selectedLocation');
		var helperSelf = this;
		
		component.set('v.showError', false);
		
		console.log('locationGeoInfo', locationGeoInfo);
		$A.util.removeClass(component.find('spinnerContainer'), 'slds-hide');
		
		action.setParams({
			north: locationGeoInfo.geometry.bounds.northeast.lat,
			south: locationGeoInfo.geometry.bounds.southwest.lat,
			east: locationGeoInfo.geometry.bounds.northeast.lng,
			west: locationGeoInfo.geometry.bounds.southwest.lng,
			isTop10: false
		});
		
		action.setCallback(this, helperSelf.handleEarthquakeResponse);
		
		$A.enqueueAction(action);
	},
	
	getLast10: function(component) {
		var action = component.get('c.getEarthquakes');
		action.setParams({
			north: 85,
			south: -85,
			east: -150,
			west: 150,
			isTop10: true
		});
		
		action.setCallback(this, function(r) {
			var response = r.getReturnValue();
			var jsonResponse = JSON.parse(response);
			
			if (jsonResponse.earthquakes) {
				component.set('v.top10', jsonResponse.earthquakes);
			}
		});
		
		$A.enqueueAction(action);
	},
	
	handleEarthquakeResponse: function(res, component) {
		var selectedLocation = component.get('v.selectedLocation');
		var response = res.getReturnValue();
		var jsonResponse = JSON.parse(response);
		var helperSelf = this;
		
		$A.util.addClass(component.find('spinnerContainer'), 'slds-hide');
		console.log('earthquakes response', jsonResponse);
		
		var mapObject = {
			center: {
				lat: selectedLocation.geometry.location.lat,
				lng: selectedLocation.geometry.location.lng,
			},
			zoom: 8
		};
		
		this.setupMap(mapObject, helperSelf.addMarkers, jsonResponse.earthquakes, component);
	},
	
	handleLocationEvent: function(component, event) {
		var locationInput = event.getParam('location');
		var action = component.get('c.geocodeLocation');
		var helperSelf = this;
		
		component.set('v.showError', false);
		
		$A.util.removeClass(component.find('spinnerContainer'), 'slds-hide');
		component.set('v.showLocations', false);
		
		action.setParams({
			locationText: locationInput
		});
		
		action.setCallback(this, function(r){
			console.log('r', r);
			var response = r.getReturnValue();
			
			if (response.isSuccess) {
				var jsonResponse = JSON.parse(response.msg);
				console.log('jsonResponse', jsonResponse);
				
				if (jsonResponse.status == 'OK' && jsonResponse.results && jsonResponse.results.length) {
					if (jsonResponse.results.length === 1) {
						component.set('v.selectedLocation', jsonResponse.results[0]);
						helperSelf.getEarthquakes(component, false);
					}
					else {
						// multiple locations found. show chooser.
						$A.util.addClass(component.find('spinnerContainer'), 'slds-hide');
						helperSelf.showLocationChooser(jsonResponse.results, component);
					}
				}
				else {
					$A.util.addClass(component.find('spinnerContainer'), 'slds-hide');
					helperSelf.displayError('There was an error finding your location. Error: ' + jsonResponse.status, component);
				}
			}
			else {
				$A.util.addClass(component.find('spinnerContainer'), 'slds-hide');
				helperSelf.displayError('There was an error finding your location. Error: ' + response.msg, component);
			}
			
		});
		
		$A.enqueueAction(action);
	},
	
	loadData: function(component) {
		var action = component.get('c.getGoogleApiKey');
		var helperSelf = this;
		
		action.setCallback(this, helperSelf.loadGoogleApi);
		
		$A.enqueueAction(action);
	},
	
	loadGoogleApi : function(result) {
		var responseObj = result.getReturnValue();
		var helperSelf = this;
		
		if (responseObj.isSuccess) {
			var apiKey = responseObj.msg;
			var script = document.createElement('script');
			
			if (script.readyState) {
				script.onreadystatechange = function() {
					if (script.readyState == 'loaded' || script.readyState == 'complete') {
						script.onreadystatechange = null;
						helperSelf.loadGoogleApiCallback();
					}
				};
			}
			else {
				script.onload = function() {
					helperSelf.loadGoogleApiCallback();
				}
			}
			
			script.src = 'https://maps.googleapis.com/maps/api/js?key=' + apiKey;
			document.getElementById('mapComponentContainer').appendChild(script);
		}
		
	},
	
	loadGoogleApiCallback: function() {
		console.log('google script loaded!!');
		
		var mapObject = {
			center: {lat: 0, lng: 0},
			zoom: 1
		};
		this.setupMap(mapObject);
	},
	
	setupMap: function(mapObject, callback, markers) {
		var map;
		map = new google.maps.Map(document.getElementById('mapContainer'), mapObject);
		
		if (callback) {
			callback(map, markers);
		}
	},
	
	showLocationChooser: function(locations, component) {
		component.set('v.locations', locations);
		component.set('v.showLocations', true);
	},
	
})