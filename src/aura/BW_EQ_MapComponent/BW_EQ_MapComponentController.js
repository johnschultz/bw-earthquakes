({
	closeNotify: function(component, event, helper) {
		component.set('v.showError', false);
		component.set('v.error', '');
	},
	
	doInit : function(component, event, helper) {
		helper.loadData(component);
		helper.getLast10(component);
	},
	
	handleLocationEvent: function(component, event, helper) {
		helper.handleLocationEvent(component, event);
	},
	
	pickLocation: function(component, event, helper) {
		var selectedLocation = event.currentTarget;
		var index = selectedLocation.dataset.index;
		var locations = component.get('v.locations');
		
		component.set('v.showLocations', false);
		component.set('v.selectedLocation', locations[index]);
		helper.getEarthquakes(component);
	}
})