({
	onKeyUp: function(component, event, helper) {
		if (event.getParam('keyCode') === 13) {
			helper.fireLocationEvent(component, event);
		}
	},
	
	submitLocation: function(component, event, helper) {
		helper.fireLocationEvent(component, event);
	}
})