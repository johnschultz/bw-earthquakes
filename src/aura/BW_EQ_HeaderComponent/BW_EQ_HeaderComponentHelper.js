({
	fireLocationEvent: function(component, event) {
		var locationInput = component.find('locationInput').getElement().value;
		
		if (locationInput) {
			var locationEvent = $A.get('e.c:BW_EQ_LocationInputEvent');
			
			locationEvent.setParams({
				'location': locationInput
			});
			
			locationEvent.fire();
		}
	}
})